const platformClient = require('purecloud-platform-client-v2');
const client = platformClient.ApiClient.instance;

const clientId = process.env.clientId;
const clientSecret = process.env.clientSecret;
const environment = process.env.environment;

exports.handler = function (event, context, callback) {
    console.log(event);

    client.setEnvironment(environment);
    console.log('Try to login to PureCloud...');

    client.loginClientCredentialsGrant(clientId, clientSecret)
        .then(function () {
            // Do authenticated things
            console.log('logged-in to PureCloud');

            let apiInstance = new platformClient.ConversationsApi();

            let body = {
                "queueId": event.queueId,
                "fromName": event.authorName,
                "subject": event.permalink,
                "direction": "INBOUND",
                "provider": "Clarabridge"
            };

            apiInstance.postConversationsEmails(body)
                .then((data) => {
                    console.log(`postConversationsEmails success!`);
                   
                    callback(null, 'Success');
                    return

                })
                .catch((err) => {
                    console.log('There was a failure calling postConversationsEmails');
                    console.error(err);
                    let error = new Error("Error");

                    callback(error);
                    return
                });

        })
        .catch((err) => {
            // Handle failure response
            console.log(err);
            let error = new Error("General error");
            callback(error);
            return 
        });
};
