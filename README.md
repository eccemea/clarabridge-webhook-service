# Clarabridge with PureCloud - Lambda AWS Function

This is webhook listener (combined with API Gateway from Amazon) which listen for events from Clarabridge Automation Recipies.
https://app.engagor.com/account/{accountId}/automation/

Defined recipies on Clarabridge sends events to this Lambda Function, where event is handled and route to PureCloud.

POST send from Clarabridge contain below variables:
*  queueId - which is queueId inside PureCloud where interaction should be routed
*  authorName - display name for an Author of a message / mention
*  permalink - direct link to the interaction on Clarabridge

above could be extended later to include other params supported for routing in PureCloud like:
*  "skillIds"
*  "languageId"
*  "priority"


